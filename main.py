import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = driver.find_element_by_id('th').text
    ah = driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-26 15:11:01.923467
#2018-02-26 15:36:01.721680
#2018-02-26 17:21:02.065583
#2018-02-26 18:19:02.013565
#2018-02-26 19:04:01.994623
#2018-02-26 20:19:01.232064
#2018-02-26 21:18:02.181077
#2018-02-26 22:18:01.382990
#2018-02-26 23:38:01.908430
#2018-02-27 01:17:02.044738
#2018-02-27 02:02:01.413767